# README #

This project shows how to use a H2 in-memory database for testing JPA entity queries. 

The H2 database is populated using a DBUnit dataset.

## Setup

To run this project you will need :  

* Java 1.8
* Maven 

## Contents

The project contains 3 JPA entities. These entities and their queries are tested in the H2Test class: 

* An Organisation entity - this entity can be associated with 0..n Locations
* A Location entity - this entity describes an office location within an Organisation. A Location can have many Employees associated with it
* A Employee entity - this entity has a link to a single Organisation and can be associated with 0..n Locations. 

The single H2Test class loads a DBUnit dataset in to a H2 in-memory database.
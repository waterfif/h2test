package com.bournbrooktech.model;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * Organisation class describes an organisation and the
 * office Locations associated with it.
 */
@NamedQueries({
        @NamedQuery(name = "findOrganisationByName", query="select o from Organisation o where o.name = :orgName"),
        @NamedQuery(name = "findAllOrganisations", query = "select o from Organisation o")})
@Entity
public class Organisation {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "ORG_NAME")
    private String name;

    @OneToMany(mappedBy="org",targetEntity=Location.class,
            fetch=FetchType.EAGER)
    private List<Location> locations;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Organisation that = (Organisation) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(locations, that.locations);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, locations);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Organisation{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}

package com.bournbrooktech.model;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * Location class describes an office location
 * the Organisation it belongs to and the Employees associated with it.
 *
 * A Location can only have 1 Organisation.
 * A Location can have many Employees associated with it and Employees
 * can be associated to 0..n Locations.
 */
@Entity
public class Location {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "LOC_NAME")
    private String locationName;

    @ManyToOne(optional = false)
    @JoinColumn(name="ORG_ID")
    private Organisation org;

    @ManyToMany(mappedBy = "locations")
    private List<Employee> employees;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public Organisation getOrg() {
        return org;
    }

    public void setOrg(Organisation org) {
        this.org = org;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return Objects.equals(id, location.id) &&
                Objects.equals(locationName, location.locationName) &&
                Objects.equals(org, location.org) &&
                Objects.equals(employees, location.employees);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, locationName, org, employees);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Location{");
        sb.append("id=").append(id);
        sb.append(", locationName='").append(locationName).append('\'');
        sb.append(", org=").append(org);
        sb.append('}');
        return sb.toString();
    }
}

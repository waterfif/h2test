package com.bournbrooktech.model;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@NamedQueries({@NamedQuery(name = "findEmployeesByOrgName", query = "select e from Employee e where e.org.name = :orgName")})
@Entity
/**
 * Employee class describes an employee, the Organisation they belong to
 * and the locations that they are associated with.
 *
 * An Employee can be associated to a single Organisation.
 * An Employee can be associated with 0..n Locations
 */
public class Employee {

    @Id
    private Long id;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name="ORG_ID")
    private Organisation org;

    @ManyToMany
    @JoinTable(
            name="EMPS_TO_LOCS",
            joinColumns=@JoinColumn(name="EMP_ID", referencedColumnName="ID"),
            inverseJoinColumns=@JoinColumn(name="LOC_ID", referencedColumnName="ID"))
    private List<Location> locations;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Organisation getOrg() {
        return org;
    }

    public void setOrg(Organisation org) {
        this.org = org;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Objects.equals(id, employee.id) &&
                Objects.equals(firstName, employee.firstName) &&
                Objects.equals(lastName, employee.lastName) &&
                Objects.equals(org, employee.org) &&
                Objects.equals(locations, employee.locations);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, org, locations);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Employee{");
        sb.append("id=").append(id);
        sb.append(", firstName='").append(firstName).append('\'');
        sb.append(", lastName='").append(lastName).append('\'');
        sb.append(", org=").append(org);
        sb.append(", locations=").append(locations);
        sb.append('}');
        return sb.toString();
    }
}

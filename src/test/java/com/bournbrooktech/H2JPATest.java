package com.bournbrooktech;

import com.bournbrooktech.model.Employee;
import com.bournbrooktech.model.Organisation;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.h2.tools.RunScript;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Standard JUnit test to test out the use of a H2 in
 * memory database with some JPA entities.
 */
public class H2JPATest {

    private static final String JDBC_DRIVER = org.h2.Driver.class.getName();
    private static final String JDBC_URL = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1";
    private static final String USER = null;
    private static final String PASSWORD = null;

    protected static EntityManagerFactory emf;
    protected static EntityManager em;

    /**
     * Build the in memory database.
     * @throws FileNotFoundException where the schema.sql cannot be found
     * @throws SQLException where the schema.sql is not valid SQL.
     */
    @BeforeClass
    public static void init() throws FileNotFoundException, SQLException {

        // Create the database using the schema file.
        RunScript.execute(
                JDBC_URL,
                USER, PASSWORD,
                "src/test/resources/schema.sql",
                StandardCharsets.UTF_8, false);

        emf = Persistence.createEntityManagerFactory("h2test-unit");
        em = emf.createEntityManager();
    }

    /**
     * Import data into the database.
     * This uses a CLEAN_INSERT which will delete any existing data
     * in the tables before inserting the data set.
     * @throws Exception where either the dataset cannot be read from the dataset file
     */
    @Before
    public void importDataSet() throws Exception {
        IDataSet dataSet = new FlatXmlDataSetBuilder()
                .build(new File("src/test/resources/dataset.xml"));
        cleanInsert(dataSet);
    }

    /*
     * Delete and then insert the data in to the given tables.
     * @param dataSet IDataSet generated from the src/test/resources/dataset.xml
     * @throws Exception  where the data set cannot be inserted in to the database.
     */
    private void cleanInsert(IDataSet dataSet) throws Exception {
        IDatabaseTester databaseTester = new JdbcDatabaseTester(JDBC_DRIVER, JDBC_URL, USER, PASSWORD);
        databaseTester.setSetUpOperation(DatabaseOperation.CLEAN_INSERT);
        databaseTester.setDataSet(dataSet);
        databaseTester.onSetup();
    }

    /**
     * Find a list of Organisation entities -
     * this should return 3 Organisations.
     */
    @Test
    public void findsAllOrganisations() {
        List<Organisation> organisations = em
                .createNamedQuery("findAllOrganisations")
                .getResultList();

        assertEquals(3, organisations.size());

        // Output the Organisations to stdout
        organisations.stream()
                .flatMap(org -> org.getLocations().stream())
                .forEach(loc -> System.out.println(String.format("Location : %s ", loc.toString())));
    }

    /**
     * Find an Organisation entity by name.
     */
    @Test
    public void findsAnOrganisationByName() {
        Organisation organisation = (Organisation) em
                .createNamedQuery("findOrganisationByName")
                .setParameter("orgName", "PACIFICA CORP")
                .getSingleResult();

        assertNotNull(organisation);

        System.out.println(String.format("Organisation : %s ", organisation.toString()));
    }

    /**
     * Find a list of Employees that belong to a given
     * Organisation. Expected 2 Employees to belong to the
     * TANIS INC organisation.
     */
    @Test
    public void findsEmployeesForAnOrganisation() {
        List<Employee> employees = em
                .createNamedQuery("findEmployeesByOrgName")
                .setParameter("orgName", "TANIS INC")
                .getResultList();

        assertEquals(2, employees.size());

        employees.stream()
                .forEach(emp -> System.out.println(String.format("Emp : %s ", emp.toString())));
    }

    /**
     * Shutdown the H2 database.
     */
    @AfterClass
    public static void tearDown(){
        em.clear();
        em.close();
        emf.close();
    }
}
